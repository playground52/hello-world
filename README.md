# Hello World

Project to test the CI-CD chain.

The Hello World application is a Web Server running on port 80 that display an Hello World page. It uses the nginx web server, according to the tutorial https://medium.com/myriatek/using-docker-to-run-a-simple-nginx-server-75a48d74500b


The ```Dockerfile``` defines the operation to replace the default ```index.html``` of the nginx image with a custom one.

The ```.gitlab-cy.yml``` contains the operations to build the Docker image, and pushes it to the Gitlab registry. It uses the docker-in-docker way as explained on https://docs.gitlab.com/ee/ci/docker/using_docker_build.html

Then it deploys the hello-world-Deployment, -Service and -Ingress to the OVH K8S cluster, leveraging Gitlab / Kubernetes integration and provided environment variables (see https://docs.gitlab.com/ee/user/project/clusters/index.html#deployment-variables).

The hello-world-LB-Service has been superseded by the hello-world-Ingress and is no longer deployed.

Starting 2021 with branch "helm-chart" the K8S deployment is governed by a chart in `helmchart` directory. The tutorial for developing it is here: https://helm.sh/docs/chart_template_guide/getting_started/


